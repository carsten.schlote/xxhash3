
# This is a demo program for the xxhash3 library

It can be compiled with all D compilers (at the moment of writting).

It output the benchmark results.

Use
```dub run --compiler=ldc2 -b release```
or
```dub run --compiler=dmd -b release```
or
```dub run --compiler=gdc -b release```
